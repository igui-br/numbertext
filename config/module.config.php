<?php
namespace NumberText;

return array(
    'view_helpers' => array(
        'invokables' => array(
            'numberText' => 'NumberText\View\Helper\NumberText'
        )
    )
);

NumberText
==========

Introduction
------------
This package can spell a number with words in different languages supported by a definition in the Soros format.

It can take a number or a money amount and convert it to a text using words of a specified language.

Many languages are supported by using language conversion definition files in the Soros format.

The class can parse a Soros file and extract the definitions to be able convert the given number in words of the specified language.
